<?php
// for gor enter 'gor' here - like below
// default theme to use
DEFINE('BRAINSTORMBLOGGER_DEFAULT_THEME', 'collab');
//error_reporting( (E_ALL ^ E_WARNING) ^ E_NOTICE);
function brainstormblogger_profile_modules() {
  $modules = array(
    // CORE MODULES
    'block', 'color', 'comment', 'help', 'menu', 'node', 'blog','taxonomy', 
    'locale', 'path', 'poll','book', 'contact', 'tracker', 'upload', 'dblog',
    'search', 'user',
    // EXTERNAL MODULES
    'better_formats', 'better_messages', 'content', 'dst', 
    'filefield', 'imageapi', 'imageapi_gd', 'imagecache', 'imagecache_ui', 'imagefield', 'lightbox2', 
    'no_anon', 'pathauto', 'persistent_login', 'tagadelic', 'token',
    'uploadpath', 'wysiwyg', 'admin_menu', 'robotstxt',
    'captcha',
    // OUR SYSTEM MODULES
    'brainstorm_update',
  );
  return $modules;
}
function brainstormblogger_profile_details() {
  return array(
    'name' => st('Brainstorm Blogger Profile'),
    'description' => st('Drupal blogger profile is intended to create blogger environment after drupal install.')
  );
}
function brainstormblogger_profile_task_list() {
  global $conf,$theme_key;
  $conf['site_name'] = 'Brainstormblogger Install';
}

function brainstormblogger_profile_final() {
    // here create beginning taxonomy and node set
}
function _brainstormblogger_theme_install(){
  if(
    isset($_POST['themelist'] )
  ){
    $theme = $_POST['themelist'];
    $themes = system_theme_data();
    if(isset($themes[$theme] ) ){
      variable_set('theme_default', $theme);
    }
  }
}
function _brainstormblogger_captcha_install(){
  if(module_exists('captcha') ){
    variable_set('captcha_add_captcha_description', 1);
    variable_set('captcha_administration_mode', 0);
    variable_set('captcha_allow_on_admin_pages', 0);
    variable_set('captcha_default_challenge', 'captcha/Math');
    variable_set('captcha_default_validation', 1);
    variable_set('captcha_description_en', 'This question is for testing whether you are a human visitor and to prevent automated spam submissions.');
    variable_set('captcha_description_ru', st('This question is for testing whether you are a human visitor and to prevent automated spam submissions.') );
    variable_set('captcha_log_wrong_responses', 0);
    variable_set('captcha_persistence', 1);
    // form settings
    db_query('DELETE FROM {captcha_points}');
    db_query("INSERT INTO {captcha_points}(form_id, module, type) VALUES('comment_form', 'captcha','Math')");
    db_query("INSERT INTO {captcha_points}(form_id, module, type) VALUES('contact_mail_user',NULL,NULL)");
    db_query("INSERT INTO {captcha_points}(form_id, module, type) VALUES('contact_mail_page', 'captcha','Math')");
    db_query("INSERT INTO {captcha_points}(form_id, module, type) VALUES('user_register', 'captcha','Math')");
    db_query("INSERT INTO {captcha_points}(form_id, module, type) VALUES('user_pass',NULL,NULL)");
    db_query("INSERT INTO {captcha_points}(form_id, module, type) VALUES('user_login',NULL,NULL)");
    db_query("INSERT INTO {captcha_points}(form_id, module, type) VALUES('user_login_block',NULL,NULL)");
    db_query("INSERT INTO {captcha_points}(form_id, module, type) VALUES('forum_node_form',NULL,NULL)");
  }
}
function _brainstormblogger_better_messages_install(){
  if(module_exists('better_messages') ){
    variable_set('better_messages_autoclose', '0');
    variable_set('better_messages_disable_autoclose', '0');
    variable_set('better_messages_fixed', '0');
    variable_set('better_messages_horizontal', '10');
    variable_set('better_messages_open_delay', '0.3');
    variable_set('better_messages_pages', '');
    variable_set('better_messages_popin_duration', 'slow');
    variable_set('better_messages_popin_effect', 'fadeIn');
    variable_set('better_messages_popout_duration', 'slow');
    variable_set('better_messages_popout_effect', 'fadeIn');
    variable_set('better_messages_pos', 'tl');
    variable_set('better_messages_vertical', '100');
    variable_set('better_messages_visibility', '0');
    variable_set('better_messages_width', '400px');
    variable_set(
      'better_messages_skin',
       drupal_get_path('module', 'better_messages') . '/skins/default/default.css'
    );
  }
}
function _brainstormblogger_dst_install(){
  if(module_exists('dst') ){
    // did not solved what to do
    variable_set('dst_used', '1');
    if(isset($_POST['dst'] ) ){
      $zl = _dst_prepare_zonelist_for_form_select();
      if(isset($zl[$_POST['dst'] ] ) ){
        variable_set('dst_used', 1);
        variable_set('dst_default_timezone', variable_get('date_default_timezone', 0) );
        variable_set('dst_default', $_POST['dst']  );
      }
      unset($zl);
    }
  }
}
function _brainstormblogger_uploadpath_install(){
  if(module_exists('uploadpath') ){
    variable_set('uploadpath_clean_filenames', '1');
    variable_set('uploadpath_excluded_node_types', array() );
    variable_set('uploadpath_prefix', '[type]/[yyyy]/[mm]');
    variable_set('uploadpath_prefix_blog', '');
    variable_set('uploadpath_prefix_book', '');
    variable_set('uploadpath_prefix_page', '');
    variable_set('uploadpath_prefix_poll', '');
    variable_set('uploadpath_prefix_story', '');
  }
}
function _brainstormblogger_upload_install($bloggers_role_id = 0){
  if(module_exists('upload') ){
    variable_set('upload_book', '1');
    variable_set('upload_extensions_default', 'jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp');
    variable_set('upload_list_default', '1');
    variable_set('upload_max_resolution', '0');
    variable_set('upload_uploadsize_default', '1');
    if($bloggers_role_id){
      variable_set('upload_extensions_' . $bloggers_role_id, 'jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp');
      variable_set('upload_uploadsize_' . $bloggers_role_id, '1');
      variable_set('upload_usersize_' . $bloggers_role_id, '1');
    }
    variable_set('upload_usersize_default', '1');
  }
}
function _brainstormblogger_filter_formats_install($bloggers_role_id  = 0){
  if($bloggers_role_id) $add_str = $bloggers_role_id . ',';
    else $add_str = '';
  db_query('UPDATE {filter_formats} SET roles=\'%s\' WHERE format=1', ',1,2,' . $add_str);
  db_query('UPDATE {filter_formats} SET roles=\'%s\' WHERE format=2', ',' . $add_str);
  variable_set('filter_default_format', '1');
  variable_set('filter_html_1', '1');
  variable_set('filter_html_help_1', 1);
  variable_set('filter_html_nofollow_1', 0);
  variable_set('filter_url_length_1', '72');
  // oh shit!11
  variable_set('allowed_html_1', '<a> <em> <strong> <blockquote> <br> <p>');
}

function _brainstormblogger_comment_install(){
  if(module_exists('comment') ){
    variable_set('comment_page', 0);
    variable_set('comment_blog', '2'); 
    variable_set('comment_default_mode_blog', '4');
    variable_set('comment_default_order_blog', '2');
    variable_set('comment_default_per_page_blog', '50');
    variable_set('comment_controls_blog', '3');
    variable_set('comment_anonymous_blog', '0');
    variable_set('comment_page', COMMENT_NODE_DISABLED);
    variable_set('comment_subject_field_blog', '1');
    variable_set('comment_preview_blog', '0');
    variable_set('comment_form_location_blog', '1');
  }
}
function _brainstormblogger_better_formats_install($bloggers_role_id = 0){
  if(module_exists('better_formats') ){
    db_query('DELETE FROM {better_formats_defaults}');
    db_query("INSERT INTO {better_formats_defaults}(rid, type, format, type_weight, weight) VALUES(1,'node',1,1,-24)");
    db_query("INSERT INTO {better_formats_defaults}(rid, type, format, type_weight, weight) VALUES(1,'comment',1,1,-24)");
    db_query("INSERT INTO {better_formats_defaults}(rid, type, format, type_weight, weight) VALUES(1,'block',0,1,25)");
    db_query("INSERT INTO {better_formats_defaults}(rid, type, format, type_weight, weight) VALUES(2,'node',1,1,-23)");
    db_query("INSERT INTO {better_formats_defaults}(rid, type, format, type_weight, weight) VALUES(2,'comment',1,1,-23)");
    db_query("INSERT INTO {better_formats_defaults}(rid, type, format, type_weight, weight) VALUES(2,'block',0,1,25)");
    if($bloggers_role_id){
      db_query(
        "INSERT INTO {better_formats_defaults}(rid, type, format, type_weight, weight) VALUES(%d,'node',2,1,-25)",
        $bloggers_role_id
      );
      db_query(
        "INSERT INTO {better_formats_defaults}(rid, type, format, type_weight, weight) VALUES(%d,'comment',2,1,-25)", 
        $bloggers_role_id
      );
      db_query(
        "INSERT INTO {better_formats_defaults}(rid, type, format, type_weight, weight) VALUES(%d,'block',0,1,25)", 
        $bloggers_role_id
      );
    }
  }
}
function _brainstormblogger_lightbox2_install(){
  if(module_exists('lightbox2') ){
    variable_set('lightbox2_border_size', '10');
    variable_set('lightbox2_box_color', 'fff');
    variable_set('lightbox2_disable_close_click', '1');
    variable_set('lightbox2_disable_resize', 0);
    variable_set('lightbox2_disable_zoom', 0);
    variable_set('lightbox2_download_link_text', 'Download Original');
    variable_set('lightbox2_enable_contact', 0);
    variable_set('lightbox2_enable_login', 0);
    variable_set('lightbox2_enable_video', 0);
    variable_set('lightbox2_fadein_speed', '0.4');
    variable_set('lightbox2_flv_player_flashvars', '');
    variable_set('lightbox2_flv_player_path', '/flvplayer.swf');
    variable_set('lightbox2_font_color', '000');
    variable_set('lightbox2_force_show_nav', 0);
    variable_set('lightbox2_imagefield_group_node_id', '1');
    variable_set('lightbox2_imagefield_use_node_title', 1);
    variable_set('lightbox2_image_count_str', 'Image !current of !total');
    variable_set('lightbox2_js_location', 'header');
    variable_set('lightbox2_keys_close', 'c x 27');
    variable_set('lightbox2_keys_next', 'n 39');
    variable_set('lightbox2_keys_play_pause', '32');
    variable_set('lightbox2_keys_previous', 'p 37');
    variable_set('lightbox2_keys_zoom', 'z');
    variable_set('lightbox2_lite', 0);
    variable_set('lightbox2_loop_items', 0);
    variable_set('lightbox2_node_link_target', 0);
    variable_set('lightbox2_node_link_text', 'View Image Details');
    variable_set('lightbox2_overlay_color', '000');
    variable_set('lightbox2_overlay_opacity', '0.8');
    variable_set('lightbox2_page_count_str', 'Page !current of !total');
    variable_set('lightbox2_page_init_action', 'page_disable');
    variable_set('lightbox2_page_list', '');
    variable_set('lightbox2_resize_sequence', '0');
    variable_set('lightbox2_resize_speed', '0.4');
    variable_set('lightbox2_slidedown_speed', '0.6');
    variable_set('lightbox2_top_position', '');
    variable_set('lightbox2_use_alt_layout', 0);
    variable_set('lightbox2_use_alt_layout', 0);
    variable_set('lightbox2_video_count_str', 'Video !current of !total');
  }
}
function _brainstormblogger_persistent_login_install(){
  if(module_exists('persistent_login') ){
    variable_set('persistent_login_maxlife', '30');
    variable_set('persistent_login_pages', '');
    variable_set('persistent_login_secure', '1');
    variable_set('persistent_login_welcome', 0);    
  }
}
function _brainstormblogger_search_install(){
  if(module_exists('search') ){
    variable_set('search_cron_limit', '100');
    variable_set('minimum_word_size', 3);
  }
}
function _brainstormblogger_set_siteinfo(){
  variable_set('site_footer', 'Drupal blogger profile by <a href="http://brainstorm.name">Ilya V. Azarov</a>');
  variable_set('site_frontpage', 'node');
  variable_set('site_slogan', '');
  variable_set('site_offline_message', st('Your site is currently under maintenance.') 
    . '<br />' 
    . '<a href="'. url('user/login') . '">' . st('Login to administer your site') . '</a>'
  );
}
function _brainstormblogger_gall_type_install(){
  if(module_exists('imagefield') ){
    $type = array(
      'type' => 'gall',
      'name' => st('Image gallery'),
      'module' => 'node',
      'description' => st("Image gallery intended  to create page to show images"),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
      'help' => '',
      'min_word_count' => '',
    );
    if(!function_exists('_node_type_set_defaults') ) include_once(drupal_get_path('module', 'node') . '/node.module');
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
    include_once(drupal_get_path('module', 'content') . '/content.crud.inc');
    include_once(drupal_get_path('module', 'content') . '/content.admin.inc');
    $field = array (
      'field_name' => 'field_images',
      'type_name' => 'gall',
      'display_settings' => array (
        'weight' => '31',
        'parent' => '',
        'label' => array (
          'format' => 'above',
        ),
        'teaser' => array (
          'format' => 'image_plain',
          'exclude' => 1,
        ),
        'full' => array (
          'format' => 'imagefield__lightbox2__preview1__original',
          'exclude' => 0,
        ),
        5 => array (
          'format' => 'image_plain',
          'exclude' => 0,
        ),
        4 => array (
          'format' => 'image_plain',
          'exclude' => 0,
        ),
        2 => array (
          'format' => 'image_plain',
          'exclude' => 0,
        ),
        3 => array (
          'format' => 'image_plain',
          'exclude' => 0,
        ),
        'token' => array (
          'format' => 'image_plain',
          'exclude' => 0,
        ),
      ),
      'widget_active' => '1',
      'type' => 'filefield',
      'required' => '0',
      'multiple' => '1',
      'db_storage' => '1',
      'module' => 'filefield',
      'active' => '1',
      'locked' => '0',
      'columns' => 
      array (
        'fid' => array (
          'type' => 'int',
          'not null' => false,
          'views' => true,
        ),
        'list' => array (
          'type' => 'int',
          'size' => 'tiny',
          'not null' => false,
          'views' => true,
        ),
        'data' => array (
          'type' => 'text',
          'serialize' => true,
          'views' => true,
        ),
      ),
      'list_field' => '0',
      'list_default' => 1,
      'description_field' => '0',
      'widget' => array (
        'file_extensions' => 'png gif jpg jpeg',
        'file_path' => '[user-id]/[site-date-yyyy]/[site-date-mm]/[site-date-dd]',
        'progress_indicator' => 'bar',
        'max_filesize_per_file' => '',
        'max_filesize_per_node' => '',
        'max_resolution' => '0',
        'min_resolution' => '0',
        'alt' => '',
        'custom_alt' => 1,
        'title' => '',
        'custom_title' => 0,
        'title_type' => 'textfield',
        'default_image' => NULL,
        'use_default_image' => 0,
        'label' => 'Add images to blog post',
        'weight' => '31',
        'description' => 'Adding iges here you can add  image gallery to post',
        'type' => 'imagefield_widget',
        'module' => 'imagefield',
      ),
    );
    content_field_instance_create($field);
  }
}
function _brainstormblogger_wysiwyg_install(){
  // Wysiwyg here. For now for FCK
  if(module_exists('wysiwyg') ) {
    $editors = wysiwyg_get_all_editors();
    db_query('DELETE FROM {wysiwyg}');
    if(isset($editors['ckeditor'] ) && $editors['ckeditor'] ['installed']) {
      // filtered html settings
      $s1 = array (
        'default' => 1,
        'user_choose' => 0,
        'show_toggle' => 1,
        'theme' => 'advanced',
        'language' => 'en',
        'buttons' => array (
          'default' => array (
            'Bold' => 1,
            'Italic' => 1,
            'Blockquote' => 1,
            'Source' => 1,
            'Cut' => 1,
            'Copy' => 1,
            'PasteText' => 1,
          ),
        ),
        'toolbar_loc' => 'top',
        'toolbar_align' => 'left',
        'path_loc' => 'bottom',
        'resizing' => 1,
        'verify_html' => 1,
        'preformatted' => 0,
        'convert_fonts_to_spans' => 1,
        'remove_linebreaks' => 1,
        'apply_source_formatting' => 0,
        'paste_auto_cleanup_on_paste' => 0,
        'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
        'css_setting' => 'none',
        'css_path' => '',
        'css_classes' => '',
      );
      $s2 = array (
        'default' => 1,
        'user_choose' => 0,
        'show_toggle' => 1,
        'theme' => 'advanced',
        'language' => 'en',
        'buttons' => array (
          'default' => array (
            'Bold' => 1,
            'Italic' => 1,
            'Underline' => 1,
            'Blockquote' => 1,
            'StrikeThrough' => 1,
            'UnorderedList' => 1,
            'OrderedList' => 1,
            'Undo' => 1,
            'Redo' => 1,
            'Link' => 1,
            'Unlink' => 1,
            'Image' => 1,
            'Source' => 1,
            'Copy' => 1,
            'Paste' => 1,
            'PasteText' => 1,
            'PasteWord' => 1,
            'SpecialChar' => 1,
            'Style' => 1,
            'Find' => 1,
            'CreateDiv' => 1,
            'Flash' => 1,
            'Table' => 1
          ),
          'tablecommands' => array (
            'TableInsertRowAfter' => 1,
            'TableInsertColumnAfter' => 1,
            'TableInsertCellAfter' => 1,
            'TableDeleteRows' => 1,
            'TableDeleteColumns' => 1,
            'TableDeleteCells' => 1,
            'TableMergeCells' => 1,
            'TableHorizontalSplitCell' => 1,
          ),
          'drupal' => array (
             'break' => 1,
          ),
        ),
        'toolbar_loc' => 'top',
        'toolbar_align' => 'left',
        'path_loc' => 'bottom',
        'resizing' => 1,
        'verify_html' => 1,
        'preformatted' => 0,
        'convert_fonts_to_spans' => 1,
        'remove_linebreaks' => 1,
        'apply_source_formatting' => 0,
        'paste_auto_cleanup_on_paste' => 0,
        'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
        'css_setting' => 'none',
        'css_path' => '',
        'css_classes' => '',
      );
      db_query(
        'INSERT INTO {wysiwyg}(format,editor,settings) VALUES(%d, \'%s\', \'%s\')',
        1, 'ckeditor', serialize($s1)
      );
      db_query(
        'INSERT INTO {wysiwyg}(format,editor,settings) VALUES(%d, \'%s\', \'%s\')',
        2, 'ckeditor', serialize($s2)
      );
    }elseif(isset($editors['fckeditor'] ) && $editors['fckeditor'] ['installed']) {
      // filtered html settings
      $s1 = array (
        'default' => 1,
        'user_choose' => 0,
        'show_toggle' => 1,
        'theme' => 'advanced',
        'language' => 'en',
        'buttons' => array (
          'default' => array (
            'Bold' => 1,
            'Italic' => 1,
            'Blockquote' => 1,
            'Source' => 1,
            'Cut' => 1,
            'Copy' => 1,
            'PasteText' => 1,
          ),
        ),
        'toolbar_loc' => 'top',
        'toolbar_align' => 'left',
        'path_loc' => 'bottom',
        'resizing' => 1,
        'verify_html' => 1,
        'preformatted' => 0,
        'convert_fonts_to_spans' => 1,
        'remove_linebreaks' => 1,
        'apply_source_formatting' => 0,
        'paste_auto_cleanup_on_paste' => 0,
        'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
        'css_setting' => 'none',
        'css_path' => '',
        'css_classes' => '',
      );
      $s2 = array (
        'default' => 1,
        'user_choose' => 0,
        'show_toggle' => 1,
        'theme' => 'advanced',
        'language' => 'en',
        'buttons' => array (
          'default' => array (
            'Bold' => 1,
            'Italic' => 1,
            'Underline' => 1,
            'StrikeThrough' => 1,
            'Blockquote' => 1,
            'UnorderedList' => 1,
            'OrderedList' => 1,
            'Undo' => 1,
            'Redo' => 1,
            'Link' => 1,
            'Unlink' => 1,
            'Image' => 1,
            'Source' => 1,
            'Copy' => 1,
            'Paste' => 1,
            'PasteText' => 1,
            'PasteWord' => 1,
            'SpecialChar' => 1,
            'Style' => 1,
            'Find' => 1,
            'CreateDiv' => 1,
            'Flash' => 1,
            'Table' => 1
          ),
          'tablecommands' => array (
            'TableInsertRowAfter' => 1,
            'TableInsertColumnAfter' => 1,
            'TableInsertCellAfter' => 1,
            'TableDeleteRows' => 1,
            'TableDeleteColumns' => 1,
            'TableDeleteCells' => 1,
            'TableMergeCells' => 1,
            'TableHorizontalSplitCell' => 1,
          ),
          'drupal' => array (
             'break' => 1,
          ),
        ),
        'toolbar_loc' => 'top',
        'toolbar_align' => 'left',
        'path_loc' => 'bottom',
        'resizing' => 1,
        'verify_html' => 1,
        'preformatted' => 0,
        'convert_fonts_to_spans' => 1,
        'remove_linebreaks' => 1,
        'apply_source_formatting' => 0,
        'paste_auto_cleanup_on_paste' => 0,
        'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
        'css_setting' => 'none',
        'css_path' => '',
        'css_classes' => '',
      );
      db_query(
        'INSERT INTO {wysiwyg}(format,editor,settings) VALUES(%d, \'%s\', \'%s\')',
        1, 'fckeditor', serialize($s1)
      );
      db_query(
        'INSERT INTO {wysiwyg}(format,editor,settings) VALUES(%d, \'%s\', \'%s\')',
        2, 'fckeditor', serialize($s2)
      );
    }
  }
}
function _brainstormblogger_pathauto_install(){
  if(module_exists('pathauto') ) {
    variable_set('pathauto_modulelist', array (
      0 => 'blog', 1 => 'node', 2 => 'taxonomy', 3 => 'tracker', 4 => 'user',
    ) );
    variable_set('pathauto_taxonomy_supportsfeeds', '0/feed');
    variable_set('pathauto_taxonomy_pattern', 'category/[tid].html');
    variable_set('pathauto_taxonomy_bulkupdate', false);
    variable_set('pathauto_taxonomy_applytofeeds', '');
    variable_set('pathauto_taxonomy_2_pattern', '');
    variable_set('pathauto_taxonomy_1_pattern', '');
    variable_set('pathauto_ignore_words', 'a,an,as,at,before,but,by,for,from,is,in,into,like,of,off,on,onto,per,since,than,the,this,that,to,up,via,with');
    variable_set('pathauto_indexaliases', false);
    variable_set('pathauto_indexaliases_bulkupdate', false);
    variable_set('pathauto_max_component_length', '100');
    variable_set('pathauto_max_length', '100');
    variable_set('pathauto_node_bulkupdate', false);
    variable_set('pathauto_node_forum_pattern', '');
    variable_set('pathauto_node_image_pattern', '');
    variable_set('pathauto_node_page_pattern', 'page/[yyyy]-[mm]-[dd]-[nid].html');
    variable_set('pathauto_node_pattern', 'content/[yyyy]-[mm]-[dd]/[nid].html');
    variable_set('pathauto_node_story_pattern', 'story/[author-uid]/[yyyy]-[mm]-[dd]/[nid].html');
    variable_set('pathauto_punctuation_quotes', false);
    variable_set('pathauto_separator', '-');
    variable_set('pathauto_update_action', '2');
    variable_set('pathauto_user_bulkupdate', '0');
    variable_set('pathauto_user_pattern', 'users/[user-raw]');
    variable_set('pathauto_user_supportsfeeds', false);
    variable_set('pathauto_verbose', '0');
    variable_set('pathauto_node_applytofeeds', '');
    variable_set('pathauto_punctuation_hyphen', '1');
    variable_set('pathauto_node_supportsfeeds', 'feed');
    variable_set('pathauto_case', '1');
    variable_set('pathauto_max_bulk_update', '50');
    variable_set('pathauto_reduce_ascii', 0);
    variable_set('pathauto_punctuation_double_quotes', '0');
    variable_set('pathauto_punctuation_backtick', '0');
    variable_set('pathauto_punctuation_comma', '0');
    variable_set('pathauto_punctuation_period', '0');
    variable_set('pathauto_punctuation_underscore', '0');
    variable_set('pathauto_transliterate', false);
    variable_set('pathauto_blog_supportsfeeds', 'feed');
    variable_set('pathauto_tracker_supportsfeeds', 'feed');
    variable_set('pathauto_punctuation_colon', '0');
    variable_set('pathauto_punctuation_semicolon', '0');
    variable_set('pathauto_punctuation_pipe', '0');
    variable_set('pathauto_punctuation_left_curly', '0');
    variable_set('pathauto_punctuation_left_square', '0');
    variable_set('pathauto_punctuation_right_curly', '0');
    variable_set('pathauto_punctuation_right_square', '0');
    variable_set('pathauto_punctuation_plus', '0');
    variable_set('pathauto_punctuation_equal', '0');
    variable_set('pathauto_punctuation_asterisk', '0');
    variable_set('pathauto_punctuation_ampersand', '0');
    variable_set('pathauto_punctuation_percent', '0');
    variable_set('pathauto_punctuation_caret', '0');
    variable_set('pathauto_punctuation_dollar', '0');
    variable_set('pathauto_punctuation_hash', '0');
    variable_set('pathauto_punctuation_at', '0');
    variable_set('pathauto_punctuation_exclamation', '0');
    variable_set('pathauto_punctuation_tilde', '0');
    variable_set('pathauto_punctuation_left_parenthesis', '0');
    variable_set('pathauto_punctuation_right_parenthesis', '0');
    variable_set('pathauto_punctuation_question_mark', '0');
    variable_set('pathauto_punctuation_less_than', '0');
    variable_set('pathauto_punctuation_greater_than', '0');
    variable_set('pathauto_punctuation_back_slash', '0');
    variable_set('pathauto_blog_pattern', 'blogs/[user-raw]');
    variable_set('pathauto_blog_bulkupdate', false);
    variable_set('pathauto_blog_applytofeeds', 'feed');
    variable_set('pathauto_node_blog_pattern', 'blog/[author-uid]/[yyyy]-[mm]-[dd]/[nid].html');
    variable_set('pathauto_node_book_pattern', 'page/[author-uid]/[yyyy]-[mm]-[dd]/[nid].html');
    variable_set('pathauto_node_poll_pattern', 'poll/[author-uid]/[yyyy]-[mm]-[dd]/[nid].html');
    variable_set('pathauto_tracker_pattern', 'users/[user-raw]/track');
    variable_set('pathauto_tracker_bulkupdate', false);
    variable_set('pathauto_tracker_applytofeeds', 'feed');
    //create alias for user/1
    if(function_exists('_pathauto_include') ){
      _pathauto_include();
      if(
        function_exists('user_pathauto_bulkupdate') 
        && function_exists('blog_pathauto_bulkupdate')
        && function_exists('tracker_pathauto_bulkupdate')
      ){
        user_pathauto_bulkupdate();
        blog_pathauto_bulkupdate();
        tracker_pathauto_bulkupdate();
      }
    }
  }
}
function _brainstormblogger_pathauto_install_final(){
  if(module_exists('pathauto') ) {// this pathauto part run final
    if(function_exists('_pathauto_include') ){
      _pathauto_include();
      if(
        function_exists('user_pathauto_bulkupdate') 
        && function_exists('blog_pathauto_bulkupdate')
        && function_exists('tracker_pathauto_bulkupdate')
      ){
        user_pathauto_bulkupdate();
        blog_pathauto_bulkupdate();
        tracker_pathauto_bulkupdate();
        node_pathauto_bulkupdate();
      }
    }
  }
}
function _brainstormblogger_imagecache_install(){
  if(module_exists('imagecache') ){
    $preset = array(
      'presetname' => 'preview1', 
    );
    imagecache_preset_save($preset);
    $presets = imagecache_presets(TRUE);
    $presetid = 0;
    if(count($presets) ) foreach($presets as $key => $p){
      if($p['presetname'] == 'preview1'){
        $presetid = $p['presetid'];
      }
    }
    if($presetid){
      db_query(
        'INSERT INTO {imagecache_action}(presetid, weight, module, action, data) 
        VALUES(%d, %d, \'%s\', \'%s\', \'%s\')',
        $presetid, 0, 'imagecache', 'imagecache_scale',
        serialize(array(
          'width' => 100,
          'height' => 100,
          'upscale' => 0,
        ))
      );
    }
    $presets = imagecache_presets(TRUE);
    $preset = imagecache_preset($presetid, $reset = TRUE);
    if(count($preset) ) imagecache_preset_flush($preset);
  }
}
function _brainstormblogger_contact_install(){
  if(module_exists('contact') ){
    db_query('DELETE FROM {contact}');
    db_query(
      'INSERT INTO {contact}(category, recipients, reply, weight,selected) VALUES(\'%s\', \'%s\', \'%s\', 0, 1)',
      st('Feedback'), variable_get('site_mail'), ''
    );
  }
}
function _brainstormblogger_taxonomy_install(){
  if(module_exists('taxonomy') ){
    $v = array(
      'name' => st('Tags'),
      'description' => st('Main tags of articles'),
      'help' => st('Enter a comma separated list of words'),
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '0',
      'required' => '0',
      'tags' => '1',
      'module' => 'taxonomy',
      'weight' => '0',
      'nodes' => array ( 'blog' => 'blog', 'story' => 'story', 'gall' => 'gall')
    );
    taxonomy_save_vocabulary($v);
  }
}
function _brainstormblogger_base_content_install(){
  // creating nodes
  $node1 = new stdClass();
  $node1->type = 'page';
  $node1->title = st('About');
  $node1->uid = 1;
  $node1->status = 1;
  $node1->promote = 0;
  $node1->format = 2;
  $node1->body = st('Hello, here you can read information about your new site on drupal<br />
You can change this page as you need, create other pages.<br /> <sup>By the way this  is example of page content type.</sup>');
  node_save($node1);
  
  $node2 = new stdClass();
  $node2->type = 'blog';
  $node2->title = st('Wellcome to brainstormblogger drupal  system');
  $node2->uid = 1;
  $node2->status = 1;
  $node2->comment = 2;
  $node2->promote = 1;
  $node2->format = 2;
  $node2->teaser = $node2->body = st('Wellcome to your new blog based on <a href="http://drupal.org/">drupal</a> content management system.<br />
Here is prepared configured system for blogging, You can blog, create own galleries. The system is already configured for more comfortable work but if you need you can customize your new blog.<br />
If you can not administer site - just login adding /user to sitename in your browser address line.
<br /><br />Profile is created by <a href="http://brainstorm.name">Ilya Azarov</a>');
  node_save($node2);
  
  // getting primary
  $prim = menu_primary_links();
  $item = array(
    'menu_name' => variable_get('menu_primary_links_source', 'primary-links'),
    'weight' => -10,
    'link_title' => st('Main'),
    'description' => st('Main page'),
    'mlid' => 0,
    'link_path' => '<front>'
  );
  menu_link_save($item);
  $item = array(
    'menu_name' => variable_get('menu_primary_links_source', 'primary-links'),
    'weight' => 0,
    'link_title' => st('About'),
    'description' => st('About this site'),
    'mlid' => 0,
    'link_path' => 'node/' . $node1->nid
  );
  menu_link_save($item);
  $item = array(
    'menu_name' => variable_get('menu_primary_links_source', 'primary-links'),
    'weight' => 10,
    'link_title' => st('Contacts'),
    'description' => st('Contact information'),
    'mlid' => 0,
    'link_path' => 'contact',
  );
  menu_link_save($item);
}
function brainstormblogger_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'install_configure') {
    // Set default for site name field.
    $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
    if(module_exists('dst') ){
      $form['server_settings'] ['date_default_timezone']['#type'] = 'hidden';
      $form['server_settings']['dst'] = array(
        '#type' => 'select',
        '#title' => st('Time zone'),
        '#default_value' => 'GMT',
        '#options' => _dst_prepare_zonelist_for_form_select(),
        '#weight' => 0,
      );
    }
    $themes =  system_theme_data();
    $form['theme_settins'] = array(
      '#type' => 'fieldset',
      '#title' => st('Select theme for your new site'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 10,
      'themelist' => array(
      )
    );
    $form['theme_settins']['themelist'] = array(
      '#prefix' => '<div style="height:400px;overflow:auto;">',
      '#suffix' => '</div>',
      '#type' => 'radios',
      '#options' => array(),
      '#default_value' => variable_get('theme_default', 'garland'),
    );
    foreach($themes as $key => $theme){
      $form['theme_settins']['themelist']['#options'] [$key] = '<strong>'
        . check_plain($theme->info['name'])
        . '</strong><br />'
        . $theme->info['description'];
      if(
        isset($theme->info['screenshot']) 
        && !empty($theme->info['screenshot']) 
      ) $form['theme_settins']['themelist']['#options'] [$key] .= 
        '<br /><img alt="" src="' . base_path() . $theme->info['screenshot'] . '" />';
    }
  }
}
function brainstormblogger_profile_tasks(&$task, $url) {
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Page'),
      'module' => 'node',
      'description' => st("A <em>page</em>, similar in form to a <em>story</em>, is a simple method for creating and displaying information that rarely changes, such as an \"About us\" section of a website. By default, a <em>page</em> entry does not allow visitor comments and is not featured on the site's initial home page."),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
      'help' => '',
      'min_word_count' => '',
    ),
    array(
      'type' => 'story',
      'name' => st('Story'),
      'module' => 'node',
      'description' => st("A <em>story</em>, similar in form to a <em>page</em>, is ideal for creating and displaying content that informs or engages website visitors. Press releases, site announcements, and informal blog-like entries may all be created with a <em>story</em> entry. By default, a <em>story</em> entry is automatically featured on the site's initial home page, and provides the ability to post comments."),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
      'help' => '',
      'min_word_count' => '',
    ),
  );
  if(!function_exists('_node_type_set_defaults') ) include_once(drupal_get_path('module', 'node') . '/node.module');
  foreach ($types as $type) {
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
  }

  // Default page to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));

  // Don't display date and author information for page nodes by default.
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  variable_set('theme_settings', $theme_settings);
  // now let's go configure modules
  // 1: user module 
  // 1.1 bloggers role
  db_query("INSERT INTO {role} (name) VALUES ('%s')", 'bloggers');
  $bloggers_role_id = 0;
  $result = db_query('SELECT rid FROM {role} WHERE name=\'%s\'', 'bloggers');
  if($o = db_fetch_object($result) ) $bloggers_role_id =  $o->rid;
  // 1.2  now make permissions - admin/user/permissions
  db_query('DELETE FROM {permission}');
  // anonym
  db_query("INSERT INTO {permission}(rid, perm, tid) VALUES(1, 'collapse format fieldset by default, collapsible format selection, show format selection for blocks, show format selection for comments, show format selection for nodes, show format tips, show more format tips link, access comments, post comments, post comments without approval, access site-wide contact form, download original image, access content, vote on polls, search content, use advanced search, view uploaded files, access user profiles',0)");
  // auth users
  db_query("INSERT INTO {permission}(rid, perm, tid)  VALUES(2,'collapse format fieldset by default, collapsible format selection, show format selection for blocks, show format selection for comments, show format selection for nodes, show format tips, show more format tips link, skip CAPTCHA, access comments, post comments, post comments without approval, access site-wide contact form, download original image, access content, vote on polls, search content, use advanced search, view uploaded files, access user profiles',0)");
  // bloggers
  db_query("INSERT INTO {permission}(rid, perm, tid)  VALUES($bloggers_role_id,'create blog entries, delete own blog entries, edit own blog entries, add content to books, skip CAPTCHA, access comments, post comments, post comments without approval, access site-wide contact form, download original image, access content, vote on polls, search content, use advanced search, upload files, view uploaded files, access user profiles, change own username',0)");
  // 1.3 make blogger from admin
  $user = user_load(array('uid' => '1') );
  $roles = $user->roles + array($bloggers_role_id => 'bloggers');
  user_save($user, array('roles' => $roles) );
  _brainstormblogger_captcha_install();
  _brainstormblogger_better_messages_install();
  _brainstormblogger_dst_install();
  // error reporting
  variable_set('error_level', '0');
  _brainstormblogger_uploadpath_install();
  _brainstormblogger_upload_install($bloggers_role_id);
  _brainstormblogger_filter_formats_install($bloggers_role_id );
  _brainstormblogger_better_formats_install($bloggers_role_id);
  _brainstormblogger_lightbox2_install();
  // perormance settings
  variable_set('cache', 1);
  variable_set('cache_lifetime', 0);
  variable_set('block_cache', 0);
  variable_set('preprocess_css', 1);
  variable_set('preprocess_js', 1);
  _brainstormblogger_persistent_login_install();
  _brainstormblogger_search_install();
  _brainstormblogger_set_siteinfo();
  _brainstormblogger_wysiwyg_install();
  _brainstormblogger_pathauto_install();
  _brainstormblogger_imagecache_install();
  _brainstormblogger_contact_install();
  _brainstormblogger_taxonomy_install();
  _brainstormblogger_gall_type_install();
  _brainstormblogger_base_content_install();
  _brainstormblogger_pathauto_install_final();
  _brainstormblogger_comment_install();
  _brainstormblogger_theme_install();
  variable_set('brainstormblogger_version', '1.01');
  menu_rebuild();
  cache_clear_all(NULL, 'cache_block');
  cache_clear_all(NULL, 'cache_content');
  cache_clear_all(NULL, 'cache_filter');
  cache_clear_all(NULL, 'cache_form');
  cache_clear_all(NULL, 'cache_menu');
  cache_clear_all(NULL, 'cache_page');
  cache_clear_all(NULL, 'cache_update');
}
